import os
import subprocess


from .._paths import __ABICC_SCRIPTS_DIR__, __ABICC_APP_DIR__
from ..linux_utils import cd


def update_app():
    """Update the app.
    """
    # cd in main repo
    with cd(__ABICC_APP_DIR__):
        # call git pull origin master
        subprocess.run(("git", "pull", "origin", "master"))
    with cd(os.path.dirname(__ABICC_APP_DIR__)):
        # install new requirements if needed
        subprocess.run(("pip3", "install", "--user", "-r", "requirements.txt"))


def update_available():
    """Checks if an update is available by calling the 'abicc-update' script.
    """
    abicc_update_script_path = os.path.join(
            __ABICC_SCRIPTS_DIR__, "abicc-check-update")
    proc = subprocess.run(("bash", abicc_update_script_path),
                          stdout=subprocess.PIPE)
    output = proc.stdout.decode("utf-8")
    if "Need to pull" in output:
        return True
    return False
