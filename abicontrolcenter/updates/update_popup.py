from kivy.properties import ObjectProperty

from .restart_required_popup import RestartRequiredPopup
from .update_utils import update_app
from ..bases import BasePopup


class UpdatePopup(BasePopup):
    """Popup layout for the update popup.
    """
    update_button = ObjectProperty(None)
    cancel_update_button = ObjectProperty(None)

    def open(self):
        super().open("Update")

    def update_app(self):
        """Update the app through the popup.
        """
        # 1. grey out the update and cancel button while the app is updating.
        self.update_button.state = "down"
        self.cancel_update_button.state = "down"
        # 2. perform update
        update_app()
        # 3. disimiss popup and raise a 'Restart required popup'
        self.dismiss()
        RestartRequiredPopup().open()
