import subprocess

from kivy.app import App

from ..bases import BasePopup


class RestartRequiredPopup(BasePopup):
    """Popup class for a restart required message that can be dismissed.
    """

    def open(self):
        super().open("Restart?")

    def restart_app(self):
        """Restart the app.
        """
        # kill app
        App.get_running_app().stop()
        subprocess.Popen("abicc")
