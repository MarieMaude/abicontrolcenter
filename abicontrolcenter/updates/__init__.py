import datetime
import json

from .update_popup import UpdatePopup
from .update_checking_popup import UpdateCheckingPopup
from .update_utils import update_available, update_app


__ALL_UPDATES_KV_FILES__ = [
        "update_popup.kv", "restart_required_popup.kv",
        "update_checking_popup.kv",
        ]
__ALL_UPDATES_SETTINGS__ = json.dumps([
    # {"type": "title",
    #  "title": "Updates"},
    {"type": "bool",
     "title": "Check for updates on opening.",
     "desc": "Check if updates are available when opening the app.",
     "section": "updates",
     "key": "updates_opening_checkup"},
    {"type": "options",
     "title": "Update frequency",
     "desc": "Frequency at which the app checks for updates.",
     "key": "updates_frequency",
     "section": "updates",
     "options": (
         "every startup", "every hour", "every day", "every week",
         "every month", "never"),
     },
    # {"type": "button",  # new type
    #  "title": "Update AbiControlCenter",
    #  "desc": "Click button to update the app:",
    #  "section": "updates",
    #  "key": "update_app",
    #  "button": {"title": "Update!", "id": "update_button"},
    # }
    ])
__ALL_UPDATES_DEFAULT_SETTINGS__ = {
        # Updates
        "updates": {
            "updates_opening_checkup": 1,
            "updates_frequency": "every startup",
            "last_update_checking": datetime.datetime.now().timestamp(),
            # "update_app": None,
            },
        }


def check_updates(update_checkup_popup):
    """Check if an update is available. If Yes, ask user to update.
    """
    if not update_checkup_popup.update_available:
        update_checkup_popup.dismiss()
        return
    update_checkup_popup.dismiss()
    # an update is available, show popup
    UpdatePopup().open()
