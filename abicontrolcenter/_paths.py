# this file contains paths for the module

import os


__ABICONTROLCENTER_PACKAGE_ROOT__ = os.path.dirname(os.path.abspath(__file__))
__MENU_BAR_PACKAGE_ROOT__ = os.path.join(
        __ABICONTROLCENTER_PACKAGE_ROOT__, "menu_bar")
__SCREENS_PACKAGE_ROOT__ = os.path.join(
        __ABICONTROLCENTER_PACKAGE_ROOT__, "screens")
__UPDATES_PACKAGE_ROOT__ = os.path.join(
        __ABICONTROLCENTER_PACKAGE_ROOT__, "updates")

__ABICC_APP_DIR__ = os.path.dirname(os.path.abspath(__file__))
__ABICC_STATIC_DIR__ = os.path.join(__ABICC_APP_DIR__, "static")
__ABICC_SOUNDS_DIR__ = os.path.join(__ABICC_STATIC_DIR__, "sounds")
__ABICC_LOGO_DIR__ = os.path.join(__ABICC_STATIC_DIR__, "logo")
__ABICC_ICON_DIR__ = os.path.join(__ABICC_STATIC_DIR__, "icons")
__ABICC_SCRIPTS_DIR__ = os.path.join(__ABICC_APP_DIR__, "scripts")
