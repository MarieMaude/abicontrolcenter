import argparse
import sys


class AbiccArgParser:
    def __init__(self):
        self.parser = self.create_parser()

    def parse_args(self):
        return self.parser.parse_args()

    def parse_known_args(self):
        return self.parser.parse_known_args()

    def create_parser(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
                "--no-allow-remotes", action="store_true",
                help="Do not allow remotes.")
        return parser


if __name__ == "__main__":
    parser = AbiccArgParser()
    args, rest = parser.parse_known_args()
    sys.argv[1:] = rest  # hack because kivy parses args directly from sys.argv
    allow_remotes = not args.no_allow_remotes

    from abicontrolcenter import AbiControlCenterApp
    AbiControlCenterApp(allow_remotes=allow_remotes).run()
