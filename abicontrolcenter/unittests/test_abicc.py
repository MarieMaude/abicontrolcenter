from .bases import BaseAbiControlCenterTest
from ..screens import __ALL_SCREENS__


class TestAbiControlCenterApp(BaseAbiControlCenterTest):
    """TestCase for the AbiControlCenter app.
    """
    def test_screen_switching(self):
        """Test we can swtich screen."""
        for screen_name in __ALL_SCREENS__:
            self.app.screen_manager.current = screen_name
            self.assertEqual(self.app.screen_manager.current, screen_name)
