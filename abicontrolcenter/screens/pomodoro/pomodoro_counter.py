from kivy.uix.label import Label


class PomodoroCounter(Label):
    """Counter label for the pomodoro counter.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pomodoro_completed = 0
        self.set_text()

    def increment(self):
        self.pomodoro_completed += 1
        self.set_text()

    def set_text(self):
        self.text = f"Pomodoro completed: {self.pomodoro_completed}"

    def reset_counter(self):
        self.pomodoro_completed = 0
        self.set_text()
