from kivy.properties import ObjectProperty

from ..bases import BaseScreen


class PomodoroScreen(BaseScreen):
    """The Pomodoro Screen object.
    """
    pomodoro_timer = ObjectProperty(None)
    pomodoro_control_bar = ObjectProperty(None)
    pomodoro_counter = ObjectProperty(None)
