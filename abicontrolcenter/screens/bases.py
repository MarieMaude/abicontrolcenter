from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen


class BaseScreen(Screen):
    """Base Screen class for the abicontrolcenter app.
    """
    menu_bar_button = ObjectProperty(None)
    """The menu bar button reference. Needed to update the text or
    other properties.
    """
