import json

from .settings_screen import (
        AbiControlCenterSettingsScreen,  # SettingsButton,
        SettingsScreen,
        )
from .custom_settings_types import SettingColorPicker, SettingScreenOrderer
from .max_window_size_exceeded_popup import MaxWindowSizeExceededPopup


__ALL_SETTINGS_KV_FILES__ = [
        "settings_screen.kv",
        "custom_settings_types.kv",
        "max_window_size_exceeded_popup.kv",
        ]
__CUSTOM_SETTINGS_TYPES__ = {
        "colorpicker": SettingColorPicker,
        "screenorderer": SettingScreenOrderer,
        }
__ALL_SETTINGS_SETTINGS__ = json.dumps([])
__ALL_SETTINGS_DEFAULT_SETTINGS__ = {}
