from ...bases import BasePopup


class MaxWindowSizeExceededPopup(BasePopup):
    """A popup that warns about the exceeded max window size.
    """
    def __init__(self, dim, *args, **kwargs):
        # do imports here to avoid import loops
        from ...app_settings import __MAX_WINDOW_WIDTH__, __MAX_WINDOW_HEIGHT__
        if dim == "width":
            self.text = f"The maximum window width is {__MAX_WINDOW_WIDTH__}."
        elif dim == "height":
            self.text = (f"The maximum window height "
                         f"is {__MAX_WINDOW_HEIGHT__}.")
        else:
            raise ValueError(dim)
        super().__init__(*args, **kwargs)

    def open(self):
        super().open("Warning!")
