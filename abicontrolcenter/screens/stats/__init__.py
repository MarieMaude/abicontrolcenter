import json

from .stats_screen import StatsScreen


__ALL_STATS_KV_FILES__ = [
        "stats_screen.kv",
        ]
__ALL_STATISTICS_SETTINGS__ = json.dumps([])
__ALL_STATISTICS_DEFAULT_SETTINGS__ = {}
