from collections import OrderedDict
import os

from .abisuite import (
        AbisuiteScreen, __ALL_ABISUITE_KV_FILES__, __ALL_ABISUITE_SETTINGS__,
        __ALL_ABISUITE_DEFAULT_SETTINGS__,
        )
from .pomodoro import (
        PomodoroScreen, __ALL_POMODORO_KV_FILES__, __ALL_POMODORO_SETTINGS__,
        __ALL_POMODORO_DEFAULT_SETTINGS__,
        )
from .settings import (
        AbiControlCenterSettingsScreen, SettingsScreen,
        __ALL_SETTINGS_KV_FILES__, __ALL_SETTINGS_SETTINGS__,
        __ALL_SETTINGS_DEFAULT_SETTINGS__,
        )
from .stats import (
        StatsScreen, __ALL_STATS_KV_FILES__, __ALL_STATISTICS_SETTINGS__,
        __ALL_STATISTICS_DEFAULT_SETTINGS__,
        )

# the key here should match the 'name' in the screen.kv file.
__ALL_SCREENS__ = OrderedDict()
__ALL_SCREENS__["abisuite"] = AbisuiteScreen
__ALL_SCREENS__["pomodoro"] = PomodoroScreen
__ALL_SCREENS__["statistics"] = StatsScreen
__ALL_SCREENS__["settings"] = SettingsScreen


__ALL_SCREENS_KV_FILES__ = (
        [os.path.join("pomodoro", filename)
         for filename in __ALL_POMODORO_KV_FILES__] +
        [os.path.join("settings", filename)
         for filename in __ALL_SETTINGS_KV_FILES__] +
        [os.path.join("abisuite", filename)
         for filename in __ALL_ABISUITE_KV_FILES__] +
        [os.path.join("stats", filename)
         for filename in __ALL_STATS_KV_FILES__]
        )

# screens settings
__ALL_SCREENS_SETTINGS__ = OrderedDict()
__ALL_SCREENS_SETTINGS__["abisuite"] = __ALL_ABISUITE_SETTINGS__
__ALL_SCREENS_SETTINGS__["pomodoro"] = __ALL_POMODORO_SETTINGS__
__ALL_SCREENS_SETTINGS__["statistics"] = __ALL_STATISTICS_SETTINGS__
__ALL_SCREENS_SETTINGS__["settings"] = __ALL_SETTINGS_SETTINGS__

# default settings
__ALL_SCREENS_DEFAULT_SETTINGS__ = OrderedDict()
for defaults in [__ALL_ABISUITE_DEFAULT_SETTINGS__,
                 __ALL_POMODORO_DEFAULT_SETTINGS__,
                 __ALL_STATISTICS_DEFAULT_SETTINGS__,
                 __ALL_SETTINGS_DEFAULT_SETTINGS__,
                 ]:
    __ALL_SCREENS_DEFAULT_SETTINGS__.update(defaults)
