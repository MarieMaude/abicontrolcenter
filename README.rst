Welcome to abicontrolcenter
===========================

This is a python package which is a WORK IN PROGRESS.
It's aim is to provide a GUI and (possibly) a webapp that will act
as a control center for abinitio calculations.


Installation
============

To install, you must have python >= 3.6 installed on your computer. You
can use for instance `anaconda <https://www.anaconda.com/>`_. Also, this is a 
git project which is hosted on gitlab. To download the project and get updates,
you need git installed on your computer. Otherwise you can just hit the download
button. If you have git installed, you can issue::

  $ git clone git@gitlab.com:fgoudreault/abicontrolcenter.git

If you downloaded the zip file instead, you can just uncompress it using
the software you want. In any case, to install the project you need first to
install the requirements using pip for example::

  $ pip install -r requirements.txt

And then, install the project::

  $ pip install -e .

To get updates (if you haven't done any other git command), just issue::

  $ git pull origin master

If you have any problems/issues/comments/ideas to improve the project or
found bugs, send
them on the `gitlab issue board <https://gitlab.com/fgoudreault/abicontrolcenter/-/issues>`_.
